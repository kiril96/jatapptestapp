//
//  LaunchViewController.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 25.02.2021.
//

import UIKit

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var vc = UIViewController()
        
        if UserService().getToken().isEmpty {
            vc = SignInViewController()
        } else {
            vc = ResultViewController()
        }
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
