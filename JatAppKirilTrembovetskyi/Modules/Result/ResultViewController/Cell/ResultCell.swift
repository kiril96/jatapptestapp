//
//  ResultCell.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 25.02.2021.
//

import UIKit

class ResultCell: UITableViewCell {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: -UPDATE CELL
    func update(character: CharacterModel) {
        symbolLabel.text = ("\(character.character)" == " " ? "Space" : "\(character.character)")
        countLabel.text = "\(character.count)"
    }
}
