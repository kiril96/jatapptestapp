//
//  ResultViewController.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 24.02.2021.
//

import UIKit

class ResultViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var characters: [CharacterModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ResultCell", bundle: nil), forCellReuseIdentifier: "ResultCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getText(locale: "en_NG")
    }

    @IBAction func logOutAction(_ sender: Any) {
        logOut()
    }
}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell") as! ResultCell
        cell.update(character: characters[indexPath.row])
        return cell
    }
}

extension ResultViewController {
    func getText(locale: String) {
        activityIndicator.startAnimating()
        RestAPIOperations().getText(locale: locale) { (model) in
            switch model {
            case .success(let result):
                if let text = result.data {
                    DispatchQueue.main.sync {
                        self.activityIndicator.stopAnimating()
                        self.setCharactersArray(text: text)
                    }
                } else {
                    DispatchQueue.main.sync {
                        self.activityIndicator.stopAnimating()
                        self.showAlert(title: "Error", message: RestMessageConstants.emptyData.rawValue)
                    }
                }
            case .failure(let error):
                debugPrint(error)
                DispatchQueue.main.sync {
                    self.activityIndicator.stopAnimating()
                    self.showAlert(title: "Error", message: error)
                }
            }
        }
    }
    
    func logOut() {
        activityIndicator.startAnimating()
        RestAPIOperations().logOut() { (model) in
            switch model {
            case .success(let result):
                if let _ = result.data {
                    UserService().clearData()
                    DispatchQueue.main.sync {
                        self.activityIndicator.stopAnimating()
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                } else {
                    DispatchQueue.main.sync {
                        self.activityIndicator.stopAnimating()
                        self.showAlert(title: "Error", message: RestMessageConstants.emptyData.rawValue)
                    }
                }
            case .failure(let error):
                debugPrint(error)
                DispatchQueue.main.sync {
                    self.activityIndicator.stopAnimating()
                    self.showAlert(title: "Error", message: error)
                }
            }
        }
    }
}

extension ResultViewController {
    func setCharactersArray(text: String) {
        let resultDict = UserService().getResultDictionaryText(gettedString: text)
        characters = []
        for (key, value) in resultDict {
            characters.append(CharacterModel(character: key, count: value))
        }
        tableView.reloadData()
    }
    
    // MARK:- Dismiss and Pop ViewControllers
    func dismissPopAllViewViewControllers() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        }
    }
}
