//
//  BaseViewController.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 24.02.2021.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController {

    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    
    var keyboardHeight: CGFloat = 0.0
    var isShowKeyboard = false
    var activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0), type: .ballClipRotate, color: .white, padding: nil)
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.frame = CGRect(x: UIScreen.main.bounds.size.width * 0.5,y: UIScreen.main.bounds.size.height * 0.5, width: 20, height: 20)
        view.addSubview(activityIndicator)
        hideKeyboardWhenTappedAround() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /// Keyboard will hide
    @objc func keyboardWillHide(_ notification : Notification) {
        if bottomViewConstraint != nil {
            bottomViewConstraint!.constant = 0
            keyboardHeight = 0
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }

    /// Keyboard will show
    @objc func keyboardWillShow(_ notification: Notification) {
        isShowKeyboard = true

        if bottomViewConstraint != nil {
            let info = notification.userInfo!
            let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

            let bottomPadding: CGFloat = {
                if #available(iOS 11.0, *) {
                    let keyWindow = UIApplication.shared.connectedScenes
                            .filter({$0.activationState == .foregroundActive})
                            .map({$0 as? UIWindowScene})
                            .compactMap({$0})
                            .first?.windows
                            .filter({$0.isKeyWindow}).first
                    return keyWindow?.safeAreaInsets.bottom ?? 0
                } else {
                    return 0
                }
            } ()


            keyboardHeight = keyboardFrame.size.height
            bottomViewConstraint.constant = keyboardHeight - bottomPadding
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
    }

    /// Hide keyboard
    @objc func hideKeyboard() {
        isShowKeyboard = false
        view.endEditing(true)
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
