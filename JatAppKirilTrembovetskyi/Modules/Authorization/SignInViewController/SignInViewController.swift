//
//  SignInViewController.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 25.02.2021.
//

import UIKit

class SignInViewController: BaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    @IBAction func logInAction(_ sender: Any) {
        logIn()
    }
    
    @IBAction func goSignUpAction(_ sender: Any) {
        let vc = SignUpViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension SignInViewController {
    
    func setup() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        emailTextField.text = String()
        passwordTextField.text = String()
        
        emailTextField.setupPlaceholder("Enter your email")
        passwordTextField.setupPlaceholder("Enter your password")
    }
    
    func pushResultController() {
        let vc = ResultViewController(nibName: "ResultViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: -DATA
extension SignInViewController {
    func logIn() {
        
        if (emailTextField.text?.isEmpty ?? true) || (passwordTextField.text?.isEmpty ?? true) {
            showAlert(title: "Error", message: RestMessageConstants.noInputData.rawValue)
        } else {
            
            activityIndicator.startAnimating()
            
            RestAPIOperations().login(email: emailTextField.text!, password: passwordTextField.text!) { (model) in
                switch model {
                case .success(let result):
                    if let data = result.data {
                        let userService = UserService()
                        userService.setUpUser(user: data)
                        userService.saveTokenData()
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.pushResultController()
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.showAlert(title: "Error", message: RestMessageConstants.emptyData.rawValue)
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        self.showAlert(title: "Error", message: error)
                    }
                }
            }
        }
    }
}

//MARK: -UITextFieldDelegate
extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            passwordTextField.resignFirstResponder()
        }
        return true
    }
}
