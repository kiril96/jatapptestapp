//
//  SignUpViewController.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 25.02.2021.
//

import UIKit

class SignUpViewController: BaseViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    @IBAction func signUpAction(_ sender: Any) {
        signUp()
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}


extension SignUpViewController {
    func setup() {
        
        nameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        nameTextField.text = String()
        emailTextField.text = String()
        passwordTextField.text = String()
        
        nameTextField.setupPlaceholder("Enter your name")
        emailTextField.setupPlaceholder("Enter your email")
        passwordTextField.setupPlaceholder("Enter your password")
    }
    
    
    func pushResultController() {
        let vc = ResultViewController(nibName: "ResultViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: -DATA
extension SignUpViewController {
    func signUp() {
        if (nameTextField.text?.isEmpty ?? true) || (emailTextField.text?.isEmpty ?? true) || (passwordTextField.text?.isEmpty ?? true) {
            showAlert(title: "Error", message: RestMessageConstants.noInputData.rawValue)
        } else {
            activityIndicator.startAnimating()
            RestAPIOperations().signUp(name: nameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!) { (model) in
                switch model {
                case .success(let result):
                    if let data = result.data {
                        let userService = UserService()
                        userService.setUpUser(userSignUp: data)
                        userService.saveTokenData()
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.pushResultController()
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            self.showAlert(title: "Error", message: RestMessageConstants.emptyData.rawValue)
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        self.showAlert(title: "Error", message: error)
                    }
                }
            }
        }
    }
}

//MARK: -UITextFieldDelegate
extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            passwordTextField.resignFirstResponder()
        }
        return true
    }
}
