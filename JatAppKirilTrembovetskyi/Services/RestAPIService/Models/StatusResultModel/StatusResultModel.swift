//
//  StatusResultModel.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 24.02.2021.
//

import UIKit

struct Resource {
    let url: URL
    let parameters: [String: Any]
    let headers: [String: String]
    let httpMethod: RestMethods
}

struct RestError: Codable {
    var name: String?
    var message: String?
    var code: Int?
    var status: Int?
}

struct BaseModel<T: Codable>: Codable {
    var status: Bool?
    var data: T?
    var errors: [RestError]?
}
enum Result<T> {
    case success(T)
    case failure(String)
}

extension URLRequest {
    
    init(_ resource: Resource) {
        self.init(url: resource.url)
        self.httpMethod = resource.httpMethod.rawValue
        self.httpBody = resource.parameters.percentEncoded()
        if resource.httpMethod != .get {
            self.httpBody = resource.parameters.percentEncoded()
        } else {
            var components = URLComponents(string: resource.url.absoluteString)!
            components.queryItems = resource.parameters.map { (key, value) in
                URLQueryItem(name: key, value: (value as! String))
            }
            self.init(url: components.url!)
        }
        self.allHTTPHeaderFields = resource.headers
    }
}



