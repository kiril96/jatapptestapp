//
//  CharacterModel.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 23.02.2021.
//

import Foundation

struct CharacterModel {
    var character: Character
    var count: Int
}
