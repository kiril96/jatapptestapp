//
//  LoginModel.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 25.02.2021.
//

import Foundation

struct LoginModel: Codable {
    var uid: Int
    var name: String
    var email: String
    var accessToken: String
    var role: Int
    var status: Int
    var createdAt: Int
    var updatedAt: Int
    
    enum CodingKeys: String, CodingKey {
        case uid, name, email, role, status
        case accessToken = "access_token"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}
