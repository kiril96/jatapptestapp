//
//  RestAPICalls.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 24.02.2021.
//

import UIKit

class RestAPICalls {
    
    func load(_ resource: Resource, result: @escaping ((Result<Data>) -> Void)) {
        
        let request = URLRequest(resource)
        
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    result(.failure(RestMessageConstants.emptyData.rawValue))
                    return
                }
                
                if let error = error {
                    result(.failure(error.localizedDescription))
                    return
                }
                result(.success(data))
            }
            task.resume()
    }
}


