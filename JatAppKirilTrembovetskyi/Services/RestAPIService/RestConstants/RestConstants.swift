//
//  RestConstants.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 24.02.2021.
//

import UIKit

enum RestMessageConstants: String {
    case emptyData = "Something went wrong. Please, try again!"
    case noInputData = "Some fields are empty, please enter all fields"
}

enum RestMethods: String {
    case get = "GET"
    case post = "POST"
}

enum RestLinks: String {
    case loginURL = "https://apiecho.cf/api/login/"
    case textURL = "https://apiecho.cf/api/get/text/"
    case signUpURL = "https://apiecho.cf/api/signup/"
    case logOutURL = "https://apiecho.cf/api/logout/"
}

enum KeychainKeys: String {
    case userTokenKey = "userTokenKey"
}

