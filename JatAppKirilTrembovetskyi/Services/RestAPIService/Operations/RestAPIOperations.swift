//
//  RestAPIOperations.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 25.02.2021.
//

import UIKit

// MARK: - LOGIN OPERATION
class RestAPIOperations {
    
    func login(email: String, password: String, _ completion: @escaping ((Result<BaseModel<LoginModel>>) -> Void)) {
        
        let parameters = ["email": email, "password": password]
        
        let resource = Resource(url: URL(string: RestLinks.loginURL.rawValue)!, parameters: parameters, headers: [:], httpMethod: .post)
        
        RestAPICalls().load(resource) { (result) in
            switch result {
            case .success(let data):
                do {
                    let item = try JSONDecoder().decode(BaseModel<LoginModel>.self, from: data)
                    completion(.success(item))
                } catch {
                    completion(.failure(RestMessageConstants.emptyData.rawValue))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

// MARK: - SIGN UP OPERATION
extension RestAPIOperations {
    
    func signUp(name: String, email: String, password: String, _ completion: @escaping ((Result<BaseModel<SignUpModel>>) -> Void)) {
        let parameters = ["name": name, "email": email, "password": password]
        let resource = Resource(url: URL(string: RestLinks.signUpURL.rawValue)!, parameters: parameters, headers: [:], httpMethod: .post)
        RestAPICalls().load(resource) { (result) in
            switch result {
            case .success(let data):
                do {
                    let item = try JSONDecoder().decode(BaseModel<SignUpModel>.self, from: data)
                    completion(.success(item))
                } catch {
                    completion(.failure(RestMessageConstants.emptyData.rawValue))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}


// MARK: - SIGN UP OPERATION
extension RestAPIOperations {
    
    func getText(locale: String, _ completion: @escaping ((Result<BaseModel<String>>) -> Void)) {
        let parameters = ["locale": locale]
        
        let resource = Resource(url: URL(string: RestLinks.textURL.rawValue)!, parameters: parameters, headers: ["Authorization": "Bearer \(UserService().getToken())"], httpMethod: .get)
        
        RestAPICalls().load(resource) { (result) in
            switch result {
            case .success(let data):
                do {
                    let item = try JSONDecoder().decode(BaseModel<String>.self, from: data)
                    completion(.success(item))
                } catch {
                    completion(.failure(RestMessageConstants.emptyData.rawValue))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

// MARK: - LOG OUT UP OPERATION
extension RestAPIOperations {
    
    func logOut(_ completion: @escaping ((Result<BaseModel<String>>) -> Void)) {
        let resource = Resource(url: URL(string: RestLinks.logOutURL.rawValue)!, parameters: [:], headers: ["Authorization": "Bearer \(UserService().getToken())"], httpMethod: .post)
        RestAPICalls().load(resource) { (result) in
            switch result {
            case .success(let data):
                do {
                    let item = try JSONDecoder().decode(BaseModel<String>.self, from: data)
                    completion(.success(item))
                } catch {
                    completion(.failure(RestMessageConstants.emptyData.rawValue))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
