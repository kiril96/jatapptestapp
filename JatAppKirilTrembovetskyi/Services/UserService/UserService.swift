//
//  UserService.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 24.02.2021.
//

import UIKit
import KeychainSwift

class UserService {
    
    private(set) var user: LoginModel?
    var keychain = KeychainSwift()
    
    func setUpUser(user: LoginModel? = nil, userSignUp: SignUpModel? = nil) {
        if let user = user {
            self.user = user
        } else if let user = userSignUp {
            self.user = LoginModel(uid: user.uid,
                                   name: user.name,
                                   email: user.email,
                                   accessToken: user.accessToken,
                                   role: user.role,
                                   status: user.status,
                                   createdAt: user.createdAt,
                                   updatedAt: user.updatedAt)
        }
    }
    
    func saveTokenData() {
        if let user = user {
            keychain.set(user.accessToken, forKey: KeychainKeys.userTokenKey.rawValue)
        }
    }
    
    func getToken() -> String {
        return keychain.get(KeychainKeys.userTokenKey.rawValue) ?? ""
    }
    
    func getResultDictionaryText(gettedString: String) -> [Character: Int] {
        let allItems = Array(gettedString)
        let uniqeItems = Set(allItems)
        var dict: [Character: Int] = [:]
        for item in uniqeItems {
            dict[item] = allItems.filter({$0 == item}).count
        }
        return dict
    }
    
    func clearData() {
        keychain.clear()
    }
}
