//
//  UITextFieldExtension.swift
//  JatAppKirilTrembovetskyi
//
//  Created by Kiril on 25.02.2021.
//

import UIKit

extension UITextField {
    func setupPlaceholder(_ text: String) {
        attributedPlaceholder = NSAttributedString(string: text,
                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                    NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Thin", size: 16)!])
    }
}
